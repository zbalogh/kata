# KATA

## Fizz Buzz

- Create a method which accepts an int as a parameter 
- In general case it returns the number itself
- For multiples of three return Fizz instead of the number
- For the multiples of five return Buzz instead of the number
- For numbers which are multiples of both three and five return FizzBuzz instead of the number

## String Calculator

### Add numbers

- Create a simple String calculator with the following method:

```java
public int add(String numbers) {
  // parsing and adding numbers
}
```
1. First iteration
- The method can take up to two numbers, separated by commas, and will return their sum **for example “” or “1” or “1,2” as inputs**.
- For an empty string it returns 0

2. Second iteration
- Add a parameter constraint to support only integers under 10 000

3. Third iteration
- Allow the Add method to handle an unknown amount of numbers

4. Fourth iteration
- Support different delimiters with adding a new parameter called **delimiter**

```java
public int add(String delimiter, String numbers) {
  // parsing and adding numbers
}
```

### Divide numbers

- Create a simple String calculator with the following method:

```java
public int divide(String numbers) {
  // parsing and adding numbers
}
```
1. First iteration
- The method can take up to two numbers, separated by commas, and will return their sum **for example “” or “1” or “1,2” as inputs**.

2. Second iteration
- Support different delimiters with adding a new parameter called **delimiter**

```java
public int divide(String delimiter, String numbers) {
  // parsing and dividing numbers
}
```

## A Song of Ice and Fire

Once upon a time there was a series of epic fantasy novels. Initially the series was envisioned as a trilogy, but the novelist finally has published five out of a planned seven volumes. People all over the world thought it was fantastic, and, of course, so did the publisher. So in a gesture of immense generosity to mankind, (and to increase sales) they set up the following pricing model to take advantage of Westeros' wonderful world.

- One copy of any of the five books costs 8 EUR. 
- If, however, you buy two different books from the series, you get a 5% discount on those two books.
- If you buy 3 different books, you get a 10% discount.
- With 4 different books, you get a 15% discount.
- If you go the whole hog, and buy all 5, you get a huge 20% discount.

**Note that if you buy, say, four books, of which 3 are different titles, you get a 10% discount on the 3 that form part of a set, but the fourth book still costs 8 EUR.**
